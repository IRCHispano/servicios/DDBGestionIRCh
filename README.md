# Gestion DDB para IRCh

Gestion DDB para IRCh es una aplicación Windows para gestionar el sistema de DDB del IRCh y compatible con el IRCd de IRC-Hispano.

Esta aplicación nace por las peticiones de ayuda de como gestionar las diferentes configuraciones del servidor de IRC que han sido creados en los últimos dias y no tener herramientas disponibles, dado que los services que se suele utilizar están completamente desfasados y no soportan las últimas actualizaciones para extender funcionamiento.


## Funcionamiento 🚀

La aplicación crea un Service pseudoservidor que se linka a un HUB de una red IRC para poder leer e insertar registros en las tablas DDB.

Se requiere tener línea C y linea H en el ircd.conf del HUB a donde se linka para poder funcionar con total normalidad.


## Configurando ⚙️

Es muy fácil de configurar, sólo pide 5 datos para conectar:

- El nombre del servidor, que tiene que coincidir con las lineas C y H del ircd.conf del servidor HUB.
- El host o la IP del servidor HUB de la red.
- El puerto del servidor HUB, suele ser el 4400.
- La contraseña de conexión que tiene que coincidir con la línea C del ircd.conf.
- El númerico de la red (NOTA, este valor no se usa aun, se utiliza el 7).


## Próximas funcionalidades 📋

Habrá unas pestañas, una para cada tabla soportada de la DDB, cada pestaña tendrá configuraciones especificas para cada tabla.

Por ejemplo, en la tabla b de bots, se mostrará los bots virtuales disponibles para configurar. En la f de features, todas las configuraciones posibles, con su valor actual, pequeño texto explicativo, etc..

Y según la acogida, se puede ir ampliando a más funciones.

¿Tienes alguna idea? ¿Sugerencia? Utiliza el apartado Incidencias/Issues del Gitlab .

