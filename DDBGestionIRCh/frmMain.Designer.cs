﻿namespace DDBGestionIRCh
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grbIRC = new System.Windows.Forms.GroupBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblEstadoTitulo = new System.Windows.Forms.Label();
            this.lblNumerico = new System.Windows.Forms.Label();
            this.txtNumerico = new System.Windows.Forms.TextBox();
            this.lblNombreServidor = new System.Windows.Forms.Label();
            this.lblPuerto = new System.Windows.Forms.Label();
            this.lblHostServidor = new System.Windows.Forms.Label();
            this.txtNombreServidor = new System.Windows.Forms.TextBox();
            this.txtPuerto = new System.Windows.Forms.TextBox();
            this.txtHostServidor = new System.Windows.Forms.TextBox();
            this.btnConectar = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtContenido = new System.Windows.Forms.TextBox();
            this.txtTabla = new System.Windows.Forms.TextBox();
            this.txtClave = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabBots = new System.Windows.Forms.TabPage();
            this.tablaBots = new DDBGestionIRCh.Tablas.Bots();
            this.tabFeatures = new System.Windows.Forms.TabPage();
            this.tabZonfig = new System.Windows.Forms.TabPage();
            this.tablaZonfig = new DDBGestionIRCh.Tablas.Zonfig();
            this.tabRaw = new System.Windows.Forms.TabPage();
            this.lblEnviar = new System.Windows.Forms.Label();
            this.lblEnviarTitulo = new System.Windows.Forms.Label();
            this.lblContenido = new System.Windows.Forms.Label();
            this.lblClave = new System.Windows.Forms.Label();
            this.lblTabla = new System.Windows.Forms.Label();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.tablaFeatures = new DDBGestionIRCh.Tablas.Features();
            this.grbIRC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabBots.SuspendLayout();
            this.tabFeatures.SuspendLayout();
            this.tabZonfig.SuspendLayout();
            this.tabRaw.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbIRC
            // 
            this.grbIRC.Controls.Add(this.lblPassword);
            this.grbIRC.Controls.Add(this.txtPassword);
            this.grbIRC.Controls.Add(this.btnDesconectar);
            this.grbIRC.Controls.Add(this.lblEstado);
            this.grbIRC.Controls.Add(this.lblEstadoTitulo);
            this.grbIRC.Controls.Add(this.lblNumerico);
            this.grbIRC.Controls.Add(this.txtNumerico);
            this.grbIRC.Controls.Add(this.lblNombreServidor);
            this.grbIRC.Controls.Add(this.lblPuerto);
            this.grbIRC.Controls.Add(this.lblHostServidor);
            this.grbIRC.Controls.Add(this.txtNombreServidor);
            this.grbIRC.Controls.Add(this.txtPuerto);
            this.grbIRC.Controls.Add(this.txtHostServidor);
            this.grbIRC.Controls.Add(this.btnConectar);
            this.grbIRC.Location = new System.Drawing.Point(12, 12);
            this.grbIRC.Name = "grbIRC";
            this.grbIRC.Size = new System.Drawing.Size(984, 89);
            this.grbIRC.TabIndex = 0;
            this.grbIRC.TabStop = false;
            this.grbIRC.Text = "Conexion a IRC";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(570, 30);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(64, 13);
            this.lblPassword.TabIndex = 6;
            this.lblPassword.Text = "Contraseña:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(636, 27);
            this.txtPassword.MaxLength = 64;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(119, 20);
            this.txtPassword.TabIndex = 7;
            this.toolTip.SetToolTip(this.txtPassword, "Contraseña de conexión, tiene que coincidir con la de la C-line del HUB.");
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.Leave += new System.EventHandler(this.txtPassword_Leave);
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.Location = new System.Drawing.Point(881, 49);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(97, 23);
            this.btnDesconectar.TabIndex = 11;
            this.btnDesconectar.Text = "DESCONECTAR";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // lblEstado
            // 
            this.lblEstado.Location = new System.Drawing.Point(66, 59);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(799, 13);
            this.lblEstado.TabIndex = 13;
            // 
            // lblEstadoTitulo
            // 
            this.lblEstadoTitulo.AutoSize = true;
            this.lblEstadoTitulo.Location = new System.Drawing.Point(6, 59);
            this.lblEstadoTitulo.Name = "lblEstadoTitulo";
            this.lblEstadoTitulo.Size = new System.Drawing.Size(43, 13);
            this.lblEstadoTitulo.TabIndex = 12;
            this.lblEstadoTitulo.Text = "Estado:";
            // 
            // lblNumerico
            // 
            this.lblNumerico.AutoSize = true;
            this.lblNumerico.Location = new System.Drawing.Point(771, 30);
            this.lblNumerico.Name = "lblNumerico";
            this.lblNumerico.Size = new System.Drawing.Size(55, 13);
            this.lblNumerico.TabIndex = 8;
            this.lblNumerico.Text = "Numerico:";
            // 
            // txtNumerico
            // 
            this.txtNumerico.Location = new System.Drawing.Point(828, 27);
            this.txtNumerico.MaxLength = 4;
            this.txtNumerico.Name = "txtNumerico";
            this.txtNumerico.Size = new System.Drawing.Size(34, 20);
            this.txtNumerico.TabIndex = 9;
            this.toolTip.SetToolTip(this.txtNumerico, "Númerico del servidor, tiene que ser único y no existente en la red.");
            this.txtNumerico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumerico_KeyPress);
            this.txtNumerico.Leave += new System.EventHandler(this.txtNumerico_Leave);
            // 
            // lblNombreServidor
            // 
            this.lblNombreServidor.AutoSize = true;
            this.lblNombreServidor.Location = new System.Drawing.Point(7, 30);
            this.lblNombreServidor.Name = "lblNombreServidor";
            this.lblNombreServidor.Size = new System.Drawing.Size(89, 13);
            this.lblNombreServidor.TabIndex = 0;
            this.lblNombreServidor.Text = "Nombre Servidor:";
            // 
            // lblPuerto
            // 
            this.lblPuerto.AutoSize = true;
            this.lblPuerto.Location = new System.Drawing.Point(471, 30);
            this.lblPuerto.Name = "lblPuerto";
            this.lblPuerto.Size = new System.Drawing.Size(41, 13);
            this.lblPuerto.TabIndex = 4;
            this.lblPuerto.Text = "Puerto:";
            // 
            // lblHostServidor
            // 
            this.lblHostServidor.AutoSize = true;
            this.lblHostServidor.Location = new System.Drawing.Point(285, 30);
            this.lblHostServidor.Name = "lblHostServidor";
            this.lblHostServidor.Size = new System.Drawing.Size(47, 13);
            this.lblHostServidor.TabIndex = 2;
            this.lblHostServidor.Text = "Host/IP:";
            // 
            // txtNombreServidor
            // 
            this.txtNombreServidor.Location = new System.Drawing.Point(98, 27);
            this.txtNombreServidor.MaxLength = 64;
            this.txtNombreServidor.Name = "txtNombreServidor";
            this.txtNombreServidor.Size = new System.Drawing.Size(175, 20);
            this.txtNombreServidor.TabIndex = 1;
            this.toolTip.SetToolTip(this.txtNombreServidor, "Nombre del servidor en la red de IRC.");
            this.txtNombreServidor.Leave += new System.EventHandler(this.txtNombreServidor_Leave);
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(514, 27);
            this.txtPuerto.MaxLength = 5;
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Size = new System.Drawing.Size(38, 20);
            this.txtPuerto.TabIndex = 5;
            this.toolTip.SetToolTip(this.txtPuerto, "Puerto del servidor HUB.");
            this.txtPuerto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPuerto_KeyPress);
            this.txtPuerto.Leave += new System.EventHandler(this.txtPuerto_Leave);
            // 
            // txtHostServidor
            // 
            this.txtHostServidor.Location = new System.Drawing.Point(334, 27);
            this.txtHostServidor.MaxLength = 64;
            this.txtHostServidor.Name = "txtHostServidor";
            this.txtHostServidor.Size = new System.Drawing.Size(124, 20);
            this.txtHostServidor.TabIndex = 3;
            this.toolTip.SetToolTip(this.txtHostServidor, "Host o IP del HUB a conectar en la red.");
            this.txtHostServidor.Leave += new System.EventHandler(this.txtHostServidor_Leave);
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(881, 19);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(97, 23);
            this.btnConectar.TabIndex = 10;
            this.btnConectar.Text = "CONECTAR";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 100;
            this.toolTip.ReshowDelay = 100;
            // 
            // txtContenido
            // 
            this.txtContenido.Location = new System.Drawing.Point(477, 44);
            this.txtContenido.Name = "txtContenido";
            this.txtContenido.Size = new System.Drawing.Size(193, 20);
            this.txtContenido.TabIndex = 15;
            this.toolTip.SetToolTip(this.txtContenido, "Contenido, si tiene algun valor, es un alta y si no tiene valor es un borrado");
            this.txtContenido.TextChanged += new System.EventHandler(this.txtContenido_TextChanged);
            // 
            // txtTabla
            // 
            this.txtTabla.Location = new System.Drawing.Point(131, 44);
            this.txtTabla.MaxLength = 1;
            this.txtTabla.Name = "txtTabla";
            this.txtTabla.Size = new System.Drawing.Size(25, 20);
            this.txtTabla.TabIndex = 11;
            this.toolTip.SetToolTip(this.txtTabla, "Tabla de la DDB, letra de la \"a\" a la \"z\"");
            this.txtTabla.TextChanged += new System.EventHandler(this.txtTabla_TextChanged);
            // 
            // txtClave
            // 
            this.txtClave.Location = new System.Drawing.Point(227, 44);
            this.txtClave.Name = "txtClave";
            this.txtClave.Size = new System.Drawing.Size(155, 20);
            this.txtClave.TabIndex = 13;
            this.toolTip.SetToolTip(this.txtClave, "Clave del registro");
            this.txtClave.TextChanged += new System.EventHandler(this.txtClave_TextChanged);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabBots);
            this.tabControl1.Controls.Add(this.tabFeatures);
            this.tabControl1.Controls.Add(this.tabZonfig);
            this.tabControl1.Controls.Add(this.tabRaw);
            this.tabControl1.Location = new System.Drawing.Point(12, 107);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(984, 331);
            this.tabControl1.TabIndex = 10;
            // 
            // tabBots
            // 
            this.tabBots.Controls.Add(this.tablaBots);
            this.tabBots.Location = new System.Drawing.Point(4, 22);
            this.tabBots.Name = "tabBots";
            this.tabBots.Padding = new System.Windows.Forms.Padding(3);
            this.tabBots.Size = new System.Drawing.Size(976, 305);
            this.tabBots.TabIndex = 0;
            this.tabBots.Text = "B - Bots";
            this.tabBots.UseVisualStyleBackColor = true;
            // 
            // tablaBots
            // 
            this.tablaBots.Location = new System.Drawing.Point(0, 0);
            this.tablaBots.Name = "tablaBots";
            this.tablaBots.Size = new System.Drawing.Size(976, 305);
            this.tablaBots.TabIndex = 0;
            this.tablaBots.Load += new System.EventHandler(this.tablaBots_Load);
            // 
            // tabFeatures
            // 
            this.tabFeatures.Controls.Add(this.tablaFeatures);
            this.tabFeatures.Location = new System.Drawing.Point(4, 22);
            this.tabFeatures.Name = "tabFeatures";
            this.tabFeatures.Size = new System.Drawing.Size(976, 305);
            this.tabFeatures.TabIndex = 10;
            this.tabFeatures.Text = "F - Features";
            this.tabFeatures.UseVisualStyleBackColor = true;
            // 
            // tabZonfig
            // 
            this.tabZonfig.Controls.Add(this.tablaZonfig);
            this.tabZonfig.Location = new System.Drawing.Point(4, 22);
            this.tabZonfig.Name = "tabZonfig";
            this.tabZonfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabZonfig.Size = new System.Drawing.Size(976, 305);
            this.tabZonfig.TabIndex = 9;
            this.tabZonfig.Text = "Z - Zonfig";
            this.tabZonfig.UseVisualStyleBackColor = true;
            // 
            // tablaZonfig
            // 
            this.tablaZonfig.Location = new System.Drawing.Point(3, 3);
            this.tablaZonfig.Name = "tablaZonfig";
            this.tablaZonfig.Size = new System.Drawing.Size(967, 296);
            this.tablaZonfig.TabIndex = 0;
            // 
            // tabRaw
            // 
            this.tabRaw.Controls.Add(this.txtContenido);
            this.tabRaw.Controls.Add(this.txtTabla);
            this.tabRaw.Controls.Add(this.lblEnviar);
            this.tabRaw.Controls.Add(this.lblEnviarTitulo);
            this.tabRaw.Controls.Add(this.lblContenido);
            this.tabRaw.Controls.Add(this.lblClave);
            this.tabRaw.Controls.Add(this.lblTabla);
            this.tabRaw.Controls.Add(this.btnEnviar);
            this.tabRaw.Controls.Add(this.txtClave);
            this.tabRaw.Location = new System.Drawing.Point(4, 22);
            this.tabRaw.Name = "tabRaw";
            this.tabRaw.Padding = new System.Windows.Forms.Padding(3);
            this.tabRaw.Size = new System.Drawing.Size(976, 305);
            this.tabRaw.TabIndex = 8;
            this.tabRaw.Text = "Manual";
            this.tabRaw.UseVisualStyleBackColor = true;
            // 
            // lblEnviar
            // 
            this.lblEnviar.Location = new System.Drawing.Point(247, 95);
            this.lblEnviar.Name = "lblEnviar";
            this.lblEnviar.Size = new System.Drawing.Size(454, 13);
            this.lblEnviar.TabIndex = 18;
            // 
            // lblEnviarTitulo
            // 
            this.lblEnviarTitulo.AutoSize = true;
            this.lblEnviarTitulo.Location = new System.Drawing.Point(170, 95);
            this.lblEnviarTitulo.Name = "lblEnviarTitulo";
            this.lblEnviarTitulo.Size = new System.Drawing.Size(71, 13);
            this.lblEnviarTitulo.TabIndex = 17;
            this.lblEnviarTitulo.Text = "Dato a enviar";
            // 
            // lblContenido
            // 
            this.lblContenido.AutoSize = true;
            this.lblContenido.Location = new System.Drawing.Point(416, 48);
            this.lblContenido.Name = "lblContenido";
            this.lblContenido.Size = new System.Drawing.Size(55, 13);
            this.lblContenido.TabIndex = 14;
            this.lblContenido.Text = "Contenido";
            // 
            // lblClave
            // 
            this.lblClave.AutoSize = true;
            this.lblClave.Location = new System.Drawing.Point(187, 48);
            this.lblClave.Name = "lblClave";
            this.lblClave.Size = new System.Drawing.Size(34, 13);
            this.lblClave.TabIndex = 12;
            this.lblClave.Text = "Clave";
            // 
            // lblTabla
            // 
            this.lblTabla.AutoSize = true;
            this.lblTabla.Location = new System.Drawing.Point(91, 48);
            this.lblTabla.Name = "lblTabla";
            this.lblTabla.Size = new System.Drawing.Size(34, 13);
            this.lblTabla.TabIndex = 10;
            this.lblTabla.Text = "Tabla";
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(715, 43);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 16;
            this.btnEnviar.Text = "ENVIAR";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // tablaFeatures
            // 
            this.tablaFeatures.Location = new System.Drawing.Point(0, 3);
            this.tablaFeatures.Name = "tablaFeatures";
            this.tablaFeatures.Size = new System.Drawing.Size(973, 302);
            this.tablaFeatures.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.grbIRC);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grbIRC.ResumeLayout(false);
            this.grbIRC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabBots.ResumeLayout(false);
            this.tabFeatures.ResumeLayout(false);
            this.tabZonfig.ResumeLayout(false);
            this.tabRaw.ResumeLayout(false);
            this.tabRaw.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbIRC;
        private System.Windows.Forms.Label lblPuerto;
        private System.Windows.Forms.Label lblHostServidor;
        private System.Windows.Forms.TextBox txtNombreServidor;
        private System.Windows.Forms.TextBox txtPuerto;
        private System.Windows.Forms.TextBox txtHostServidor;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.Label lblNumerico;
        private System.Windows.Forms.TextBox txtNumerico;
        private System.Windows.Forms.Label lblNombreServidor;
        private System.Windows.Forms.Button btnDesconectar;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblEstadoTitulo;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabBots;
        private System.Windows.Forms.TabPage tabRaw;
        private System.Windows.Forms.TextBox txtContenido;
        private System.Windows.Forms.TextBox txtTabla;
        private System.Windows.Forms.Label lblEnviar;
        private System.Windows.Forms.Label lblEnviarTitulo;
        private System.Windows.Forms.Label lblContenido;
        private System.Windows.Forms.Label lblClave;
        private System.Windows.Forms.Label lblTabla;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox txtClave;
        private System.Windows.Forms.TabPage tabFeatures;
        private System.Windows.Forms.TabPage tabZonfig;
        private Tablas.Zonfig tablaZonfig;
        private Tablas.Bots tablaBots;
        private Tablas.Features tablaFeatures;
    }
}

