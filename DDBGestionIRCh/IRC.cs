﻿/*
 * DDBGestionIRCh, IRC.cs
 *
 * Copyright (C) 2020 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DDBGestionIRCh
{
    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 409600;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }

    public enum EstadoIRC
    {
        Desconectado,
        Conectando,
        Leyendo,
        Listo
    }
    public class IRC
    {
        private static Socket sockIRC;
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);
        private bool conectado;

        private static string myName;
        private static string myNumeric;
        private static string serverHubName;
        private static string serverHubNumerico;
        private static DDB ddb;
        public DDB DDB { get { return ddb; } }

        private static string ultimoError;
        private static string ultimaAccion;

        public delegate void IRCEvent(EstadoIRC estado, string msg);
        public delegate void IRCEventMessage(string msg);
        public event IRCEvent CambioEstadoIRC;
        public event IRCEventMessage MsgIn;
        public event IRCEventMessage MsgOut;


        #region Eventos

        protected virtual void AvisarCambioEstadoIRC(EstadoIRC estado, string msg)
        {
            IRCEvent tmp = CambioEstadoIRC;
            if (tmp != null)
                tmp(estado, msg);
        }

        protected virtual void LogMsgIn(string msg)
        {
            IRCEventMessage tmp = MsgIn;
            if (tmp != null)
                tmp(msg);
        }

        protected virtual void LogMsgOut(string msg)
        {
            IRCEventMessage tmp = MsgOut;
            if (tmp != null)
                tmp(msg);
        }
        #endregion

        public string UltimoError { get { return ultimoError; } }
        public string UltimaAccion { get { return ultimaAccion; } }

        public IRC()
        {
            sockIRC = new Socket(SocketType.Stream, ProtocolType.Tcp);
            ddb = new DDB(this);      
        }

        public bool ConectarIRC(string servidor, string hostServidor, short puerto, string password, short numerico)
        {
            try
            {
                ultimoError = "";
                myNumeric = "H";
                myName = servidor;

                AvisarCambioEstadoIRC(EstadoIRC.Conectando, "Conectando...");

                // Establecemos la conexion
                sockIRC.BeginConnect(hostServidor, puerto, new AsyncCallback(ConnectCallback), sockIRC);
                connectDone.WaitOne();

                AvisarCambioEstadoIRC(EstadoIRC.Leyendo, "Bursting...");

                long epoch = DateTimeOffset.Now.ToUnixTimeSeconds();

                Send(sockIRC, String.Format("PASS :{0}", password));
                Send(sockIRC, String.Format("SERVER {0} 1 {2} {2} J10 {3} +hs6 :Gestion de DDB para IRCh",
                        servidor, epoch, epoch, myNumeric + (myNumeric.Length == 2 ? "]]]" : "]]")));

                ddb.DDB_Burst();

                Send(sockIRC, String.Format("{0} N GestionDDB 1 {1} - - +i {2} :Bot de GestionDDB", myNumeric, epoch, myNumeric + (myNumeric.Length == 2 ? "AAA" : "AA")));
                Send(sockIRC, String.Format("{0} EB", myNumeric));

                sendDone.WaitOne();

                //Recibir respuesta del servidor
                Receive(sockIRC);
                //receiveDone.WaitOne();

                AvisarCambioEstadoIRC(EstadoIRC.Listo, "Listo");

                conectado = true;
                ultimaAccion = "Conectado";

                return conectado;
            }
            catch (Exception ex)
            {
                /* Se produjo un error durante el intento de conexión ya que la parte conectada no respondió adecuadamente tras un periodo de tiempo, o bien se produjo un error en la conexión establecida ya que el host conectado no ha podido responder [::ffff:1.2.3.4]:4400 */
                /* Host desconocido */
                /*La dirección solicitada no es válida en este contexto [::ffff:234.33.21.23]:4400 */
                AvisarCambioEstadoIRC(EstadoIRC.Desconectado, "Error al conectar...");
                ultimoError = ex.Message;
                return false;
            }
        }

        public bool DesconectarIRC()
        {
            try
            {
                Send(sockIRC, String.Format("{0} SQ {1} 0 :Cierre de la Aplicacion", "H", "black.hole"));
                //sockIRC.Shutdown(SocketShutdown.Both);
                //sockIRC.Close();
                sockIRC.Disconnect(true);

                AvisarCambioEstadoIRC(EstadoIRC.Desconectado, "Desconectado");
                ultimaAccion = "Desconectado";
                return true;
            }
            catch (Exception ex)
            {
                ultimoError = ex.Message;
                return false;
            }
        }

        public void EnviarRaw(string texto)
        {
            Send(sockIRC, texto);
            Receive(sockIRC);
        }

        public void EnviarComando(string texto)
        {
            string linea;
            linea = String.Format("{0} {1}", myNumeric, texto);

            Send(sockIRC, linea);
        }

        public void EnviarDB(char tabla, string clave, string contenido)
        {
            string lineaDB;

            ddb.id_table[tabla]++;

            if (contenido != null && contenido.Length > 0)
                lineaDB = String.Format("{0} DB * {1} {2} {3} :{4}", myNumeric, ddb.id_table[tabla], tabla, clave, contenido);
            else
                lineaDB = String.Format("{0} DB * {1} {2} {3}", myNumeric, ddb.id_table[tabla], tabla, clave);

            Send(sockIRC, lineaDB);
        }

        public ulong GetNumeroSerieTabla(char tabla)
        {
            return ddb.id_table[tabla];
        }
     

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);
                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void Receive(Socket client)
        {
            try
            {
                StateObject state = new StateObject();
                state.workSocket = client;

                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                    ParseIRC(state.sb.ToString());
                    state.sb.Clear();

                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    if (state.sb.Length > 1)
                    {
                        ParseIRC(state.sb.ToString());
                        state.sb.Clear();
                    }
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void Send(Socket client, String data)
        {
            //LogMsgOut(data);

            data = data + Environment.NewLine;

            byte[] byteData = Encoding.ASCII.GetBytes(data);

            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;

                int bytesSent = client.EndSend(ar);

                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ParseIRC(string raw)
        {
            string[] lineas = raw.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            //LogMsgIn(raw);

            foreach (string linea in lineas)
            {
                string[] parametros;

                //Casos Especiales
                //:server CONFIG, es un fallo del ircd.
                if (linea[0] == ':')
                    continue;

                parametros = SplitIRC(linea);

                //ERROR
                if (parametros[0].Equals("ERROR")) {
                    //AvisarCambioEstadoIRC(EstadoIRC.Desconectado, "Error al conectar...");
                    ultimoError = parametros[1];
                }


                //PASS
                if (parametros[0].Equals("PASS"))
                    continue;

                //SERVIDOR al inicio
                if (parametros[0].Equals("SERVER"))
                {
                    serverHubName = parametros[1];
                    serverHubNumerico = parametros[6].Substring(0, parametros[6].Length == 5 ? 2 : 1);
                    continue;
                }

                //Parseo de comandos de irc
                if (parametros[1].Equals("DB"))
                {
                    ddb.DDB_DB(parametros);
                    // Parseamos los registros DB

                    /* Burst
                     * 0 X
                     * 1 DB (comando)
                     * 2 * (destino)
                     * 3 0 
                     * 4 J (Join Burst)
                     * 5 218 (serie)
                     * 6 2 (tabla, 2 para n)
                     */

                    //TODO: Cuando leamos los valores, mandar los J a 0.
                    /*
                    if (parametros[6].Equals("2"))
                        ddb.id_table['n'] = ulong.Parse(parametros[5]);
                    else
                        ddb.id_table[parametros[6][0]] = ulong.Parse(parametros[5]);
                        */
                    continue;
                }

                if (parametros[1].Equals("EB"))
                {
                    if (parametros[0].Equals(serverHubNumerico))
                    {
                        Send(sockIRC, String.Format("{0} EA", myNumeric));
                    }
                    continue;
                }

                if (parametros[1].Equals("V"))
                {
                    Send(sockIRC, String.Format("{0} 351 {1} {2} :GestionDDB Fecha: {3}", myNumeric, Application.ProductVersion, myName, File.GetLastWriteTime(Assembly.GetEntryAssembly().Location).ToShortDateString()));
                    continue;
                }

                if (parametros[1].Equals("G"))
                {
                    Send(sockIRC, String.Format("{0} Z :{1}", myNumeric, parametros[2]));
                    continue;
                }
            }
        }

        private static string[] SplitIRC(string linea)
        {
            List<string> partes;

            //Se hace split hasta el primer :
            string[] textoLargo = linea.Split(':');

            //Ahora dividimos en partes
            string[] parametros = textoLargo[0].Split(' ');
            partes = parametros.ToList();
            partes.Remove("");
            if (textoLargo.Length > 1)
                partes.Add(linea.Substring(textoLargo[0].Length + 1));

            return partes.ToArray();
        }
    }
}
