﻿namespace DDBGestionIRCh.Tablas
{
    partial class Bots
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.dgvTablaBots = new System.Windows.Forms.DataGridView();
            this.colBotVirtual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMascara = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGuardar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colEliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnRefrescar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaBots)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(20, 17);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(248, 13);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "La tabla b de BOTS se gestionan los bots virtuales.";
            // 
            // dgvTablaBots
            // 
            this.dgvTablaBots.AllowUserToAddRows = false;
            this.dgvTablaBots.AllowUserToDeleteRows = false;
            this.dgvTablaBots.AllowUserToOrderColumns = true;
            this.dgvTablaBots.AllowUserToResizeColumns = false;
            this.dgvTablaBots.AllowUserToResizeRows = false;
            this.dgvTablaBots.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTablaBots.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colBotVirtual,
            this.colDescripcion,
            this.colMascara,
            this.colGuardar,
            this.colEliminar});
            this.dgvTablaBots.Location = new System.Drawing.Point(23, 48);
            this.dgvTablaBots.MultiSelect = false;
            this.dgvTablaBots.Name = "dgvTablaBots";
            this.dgvTablaBots.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTablaBots.Size = new System.Drawing.Size(916, 150);
            this.dgvTablaBots.TabIndex = 6;
            this.dgvTablaBots.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaBots_CellClick);
            this.dgvTablaBots.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaBots_CellEndEdit);
            this.dgvTablaBots.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaBots_CellValueChanged);
            // 
            // colBotVirtual
            // 
            this.colBotVirtual.HeaderText = "Bot Virtual";
            this.colBotVirtual.Name = "colBotVirtual";
            this.colBotVirtual.ReadOnly = true;
            // 
            // colDescripcion
            // 
            this.colDescripcion.HeaderText = "Descripción";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            this.colDescripcion.Width = 500;
            // 
            // colMascara
            // 
            this.colMascara.HeaderText = "Máscara";
            this.colMascara.MaxInputLength = 115;
            this.colMascara.Name = "colMascara";
            this.colMascara.ToolTipText = "Máscara nick!user@host del bot virtual";
            this.colMascara.Width = 170;
            // 
            // colGuardar
            // 
            this.colGuardar.HeaderText = "Guardar";
            this.colGuardar.Name = "colGuardar";
            this.colGuardar.Text = "Guardar";
            this.colGuardar.Width = 50;
            // 
            // colEliminar
            // 
            this.colEliminar.HeaderText = "Eliminar";
            this.colEliminar.Name = "colEliminar";
            this.colEliminar.Width = 50;
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Location = new System.Drawing.Point(148, 222);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(88, 23);
            this.btnRefrescar.TabIndex = 7;
            this.btnRefrescar.Text = "REFRESCAR";
            this.btnRefrescar.UseVisualStyleBackColor = true;
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // Bots
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRefrescar);
            this.Controls.Add(this.dgvTablaBots);
            this.Controls.Add(this.lblTitulo);
            this.Name = "Bots";
            this.Size = new System.Drawing.Size(952, 259);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaBots)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridView dgvTablaBots;
        private System.Windows.Forms.Button btnRefrescar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBotVirtual;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMascara;
        private System.Windows.Forms.DataGridViewButtonColumn colGuardar;
        private System.Windows.Forms.DataGridViewButtonColumn colEliminar;
    }
}
