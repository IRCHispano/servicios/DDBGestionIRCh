﻿namespace DDBGestionIRCh.Tablas
{
    partial class Features
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.dgvTablaFeatures = new System.Windows.Forms.DataGridView();
            this.colFeature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGuardar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colEliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnRefrescar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaFeatures)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(20, 17);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(506, 13);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "La tabla f de FEATURES se gestionan las características configurables sobre el fu" +
    "ncionamiento del IRCh.";
            // 
            // dgvTablaFeatures
            // 
            this.dgvTablaFeatures.AllowUserToAddRows = false;
            this.dgvTablaFeatures.AllowUserToDeleteRows = false;
            this.dgvTablaFeatures.AllowUserToOrderColumns = true;
            this.dgvTablaFeatures.AllowUserToResizeColumns = false;
            this.dgvTablaFeatures.AllowUserToResizeRows = false;
            this.dgvTablaFeatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTablaFeatures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFeature,
            this.colDescripcion,
            this.colValor,
            this.colGuardar,
            this.colEliminar});
            this.dgvTablaFeatures.Location = new System.Drawing.Point(23, 48);
            this.dgvTablaFeatures.MultiSelect = false;
            this.dgvTablaFeatures.Name = "dgvTablaFeatures";
            this.dgvTablaFeatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTablaFeatures.Size = new System.Drawing.Size(916, 150);
            this.dgvTablaFeatures.TabIndex = 6;
            this.dgvTablaFeatures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaFeatures_CellClick);
            this.dgvTablaFeatures.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaFeatures_CellEndEdit);
            this.dgvTablaFeatures.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaFeatures_CellValueChanged);
            // 
            // colFeature
            // 
            this.colFeature.HeaderText = "Feature";
            this.colFeature.Name = "colFeature";
            this.colFeature.ReadOnly = true;
            this.colFeature.Width = 160;
            // 
            // colDescripcion
            // 
            this.colDescripcion.HeaderText = "Descripción";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            this.colDescripcion.Width = 430;
            // 
            // colValor
            // 
            this.colValor.HeaderText = "Valor";
            this.colValor.MaxInputLength = 115;
            this.colValor.Name = "colValor";
            this.colValor.ToolTipText = "Valor del feature";
            this.colValor.Width = 160;
            // 
            // colGuardar
            // 
            this.colGuardar.HeaderText = "Guardar";
            this.colGuardar.Name = "colGuardar";
            this.colGuardar.Text = "Guardar";
            this.colGuardar.Width = 50;
            // 
            // colEliminar
            // 
            this.colEliminar.HeaderText = "Eliminar";
            this.colEliminar.Name = "colEliminar";
            this.colEliminar.Width = 50;
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Location = new System.Drawing.Point(148, 222);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(88, 23);
            this.btnRefrescar.TabIndex = 7;
            this.btnRefrescar.Text = "REFRESCAR";
            this.btnRefrescar.UseVisualStyleBackColor = true;
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // Features
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRefrescar);
            this.Controls.Add(this.dgvTablaFeatures);
            this.Controls.Add(this.lblTitulo);
            this.Name = "Features";
            this.Size = new System.Drawing.Size(952, 259);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaFeatures)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridView dgvTablaFeatures;
        private System.Windows.Forms.Button btnRefrescar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFeature;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValor;
        private System.Windows.Forms.DataGridViewButtonColumn colGuardar;
        private System.Windows.Forms.DataGridViewButtonColumn colEliminar;
    }
}
