﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DDBGestionIRCh.Tablas
{
    public class ClavesTablaBots
    {
        public string botVirtual;
        public string descripcionBot;
        public string mascara;
        public bool changed;

        public ClavesTablaBots(string botVirtual, string descripcion)
        {
            this.botVirtual = botVirtual;
            this.descripcionBot = descripcion;
            this.mascara = null;
            this.changed = false;
        }
    }

    public enum TiposClaveTablaFeatures
    {
        STRING,
        BOOL,
        UINT,
        INT,
        CHANNEL
    }

    public class ClavesTablaFeatures
    {
        public string feature;
        public string descripcionFeature;
        public TiposClaveTablaFeatures tipoFeature;
        public string valor;
        public bool onlyIRCh;
        public bool changed;

        public ClavesTablaFeatures(string feature, string descripcion, TiposClaveTablaFeatures tipo, bool onlyIRCh)
        {
            this.feature = feature;
            this.descripcionFeature = descripcion;
            this.tipoFeature = tipo;
            this.valor = null;
            this.onlyIRCh = onlyIRCh;
            this.changed = false;
        }
    }

    public class ClavesTablaZonfig
    {
        public string tipoLogging;
        public string descripcionLogging;
        public string canal;
        public bool changed;

        public ClavesTablaZonfig(string tipo, string descripcion)
        {
            this.tipoLogging = tipo;
            this.descripcionLogging = descripcion;
            this.canal = null;
            this.changed = false;
        }
    }
    public static class ClavesDDB
    {

    }
}
