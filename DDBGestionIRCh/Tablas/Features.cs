﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DDBGestionIRCh.Tablas
{
    public partial class Features : UserControl
    {
        private ClavesTablaFeatures[] listaFeatures;
        private IRC irc;
        public Features()
        {
            InitializeComponent();

            listaFeatures = new ClavesTablaFeatures[] 
            {
                new ClavesTablaFeatures("DOMAINNAME", "Dominio de los servidores de la red", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("RELIABLE_CLOCK", "Reloj del sistema es estable y precisa", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("BUFFERPOOL", "Bytes máximo de memoria para los enlaces de servidor", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("HAS_FERGUSON_FLUSHER", "Intentar eliminar buffers antes de tirar clientes", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CLIENT_FLOOD", "Bytes de la cola de envio de cliente al servidor", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("SERVER_PORT", "Puerto de servidor por defecto", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("NODEFAULTMOTD", "No enviar el MOTD al cliente", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("MOTD_BANNER", "Si habilita NODEFAULTMOTD, mensaje que se enviaría", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("PROVIDER", "Nombre del proveedor del servidor, sale en el 001", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("KILL_IPMISMATCH", "Killear si no coincide DNS inverso con el directo", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("IDLE_FROM_MSG", "Solo resetear IDLE con el PRIVMSG", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HUB", "Define el servidor como HUB", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("WALLOPS_OPER_ONLY", "Desactiva la recepción de WALLOPS por usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("NODNS", "No hacer la comprobación DNS", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("NOIDENT", "No hacer la comprobación IDENT", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("DEFAULT_LIST_PARAM", "Parámetros por defecto del /LIST", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("NICKNAMEHISTORYLENGTH", "Tamaño del historial de WHOWAS", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("AUTOUSERMODES", "Modos por defecto de un usuario", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("HOST_HIDING", "Usuarios pueden ponerse el +x", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIDDEN_HOST", "Dominio de host virtual para los +x", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("HIDDEN_IP", "IP fake para el /USERIP en usuarios con +x", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("CONNEXIT_NOTICES", "Mostrar notice cuando se [des]conectan usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("OPLEVELS", "Permite usar los modos +A y +U de canales", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("ZANNELS", "Permite canales persistentes", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("LOCAL_CHANNELS", "Permite a los usuarios crear canales locales", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("TOPIC_BURST", "Habilita el burst de Topicos en el net.burst", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("USER_GLIST", "Habilita a los usuarios usar el /GLINE", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("DISABLE_GLINES", "Desactiva el sistema de G-Lines", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("MSG_GLINED", "Mensaje que se mostrará al Glineado", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("MSG_QUIT", "Mensaje de quit personalizado", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("MSG_PART", "Mensaje de part personalizado", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("KILLCHASETIMELIMIT", "Segundos de persecucción del KILL", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("MAXCHANNELSPERUSER", "Número máximo de canales por usuario", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("NICKLEN", "Longitud máxima del nick", TiposClaveTablaFeatures.UINT, false),
                new ClavesTablaFeatures("AVBANLEN", "Longitud media de máscara de ban", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("MAXBANS", "Máximo número de bans por canal", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("MAXSILES", "Máximo número de silences por usuario", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("MAXMONITOR", "Máximo número de nicks monitorizados por usuario", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("HANGONGOODLINK", "Segundos con conexion abierta antes de reintentar", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("HANGONRETRYDELAY", "Segundos para restablecer una conexión con servidores", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("CONNECTTIMEOUT", "Segundos para completar conexión de un cliente", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("MAXIMUM_LINKS", "Máximo número de links para Clase 0", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("PINGFREQUENCY", "Segundos para PING para Clase 0", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("CONNECTFREQUENCY", "Segundos para frecuencia de reconexiones para Clase 0", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("DEFAULTMAXSENDQLENGTH", "Bytes de MaxSendQ para Clase 0", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("GLINEMAXUSERCOUNT", "Máximo de usuarios afectados para permitir una G-line", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SOCKSENDBUF", "Bytes de buffer para envío a otros servidores", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SOCKRECVBUF", "Bytes de buffer para recepción de otros servidores", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_CLONE_LIMIT", "Número de veces que se puede conectar antes de Throttle", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_CLONE_PERIOD", "Segundos para la comprobación del Throttle", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_48_CLONE_LIMIT", "Número de veces que se puede conectar antes de Throttle para /48", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_48_CLONE_PERIOD", "Segundos para la comprobacion del Throttle para /48", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_CLONE_DELAY", "Segundos de espera para conectar cuando hay Throttle", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IPCHECK_DEFAULT_ILINE", "Número de conexiones simultáneas por defecto", TiposClaveTablaFeatures.INT, false),
                new ClavesTablaFeatures("IPCHECK_MSG_TOOMANY", "Mensaje por demasiadas conexiones simultáneas", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("CHANNELLEN", "Longitud máxima del nombre de canal", TiposClaveTablaFeatures.UINT, true),
                new ClavesTablaFeatures("MPATH", "Nombre de archivo del MOTD", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("RPATH", "Nombre de archivo del MOTD remoto", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("PPATH", "Nombre de archivo del PID", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("DDBPATH", "Nombre del directorio para la DDB", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("ZLIB_CMPRS_SERVER", "Tipo de compresión para enlaces entre servidores", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("ZLIB_CMPRS_CLIENT", "Tipo de compresión para enlaces con clientes", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("TOS_SERVER", "Tipo de TOS para enlaces entre servidores", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("TOS_CLIENT", "Tipo de TOS para enlaces con clientes", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("POLLS_PER_LOOP", "Número de eventos simultáneos de kernel", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IRCD_RES_RETRIES", "Número de intentos para resolución DNS", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("IRCD_RES_TIMEOUT", "Segundos de espera para resolución DNS", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("AUTH_TIMEOUT", "Segundos de espera máxima para la resolución IDENT y DNS", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("ANNOUNCE_INVITES", "Mostrar texto a OPs cuando se invita al canal", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CONFIG_OPERCMDS", "Se permite hacer cambios entre servidores", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_SNOTICES", "No se permite recibir SNOTICES por los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_SNOTICES_OPER_ONLY", "No se permite poner modo +s a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_DEBUG_OPER_ONLY", "No se permite recibir WALLOPS (+g) de servidores a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WALLOPS", "No se permite recibir WALLOPS de operadores a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_MAP", "No se permite usar el /MAP a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_LINKS", "No se permite usar el /LINKS a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_TRACE", "No se permite usar el /TRACE a los usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_NAMESERVERS", "No se permite usar el /STATS a a los usuarios para DNS", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_DDB", "No se permite usar el /STATS b a los usuarios para DDB", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_CONNECT", "No se permite usar el /STATS c a los usuarios para Conexiones entre servidores", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_CRULES", "No se permite usar el /STATS d a los usuarios para Reglas", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_ENGINE", " No se permite usar el /STATS e a los usuarios para Engines", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_ELINES", "No se permite usar el /STATS E a los usuarios para Elines", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_FEATURES", "No se permite usar el /STATS f a los usuarios para Features", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_FEATURESALL", "No se permite usar el /STATS F a los usuarios para todas Features", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_GLINES", "No se permite usar el /STATS g a los usuarios para Glines", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_ACCESS", "No se permite usar el /STATS i a los usuarios para Accesos", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_HISTOGRAM", "No se permite usar el /STATS h a los usuarios para Histogramas", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_JUPES", "No se permite usar el /STATS j a los usuarios para Jupes", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_KLINES", "No se permite usar el /STATS k a los usuarios para Klines", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_LINKS", "No se permite usar el /STATS l a los usuarios para Links", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_MODULES", "No se permite usar el /STATS L a los usuarios para Modulos", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_COMMANDS", "No se permite usar el /STATS m a los usuarios para Comandos", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_OPERATORS", "No se permite usar el /STATS o a los usuarios para Operadores", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_PORTS", "No se permite usar el /STATS p a los usuarios para Puertos", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_QUARANTINES", "No se permite usar el /STATS q a los usuarios para Cuarantenas", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_MAPPINGS", "No se permite usar el /STATS R a los usuarios para Mapeos", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_USAGE", "No se permite usar el /STATS r a los usuarios para Usage", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_LOCALS", "No se permite usar el /STATS t a los usuarios para Locales", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_MOTDS", "No se permite usar el /STATS T a los usuarios para MOTDs", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_UPTIME", "No se permite usar el /STATS u a los usuarios para Uptime", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_UWORLD", "No se permite usar el /STATS U a los usuarios para Uworld", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_VSERVERS", "No se permite usar el /STATS v a los usuarios para Vservers", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_USERLOAD", "No se permite usar el /STATS w a los usuarios para Userload", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_WEBIRC", "No se permite usar el /STATS W a los usuarios para WEBIRC", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_MEMUSAGE", "No se permite usar el /STATS x a los usuarios para Uso Memoria", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_CLASSES", "No se permite usar el /STATS y a los usuarios para Clases", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_MEMORY", "No se permite usar el /STATS z a los usuarios para Memoria", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_STATS_IAUTH", "No se permite usar el /STATS IAUTH y /STATS IAUTHCONF a los usuarios para IAUTH", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WEBIRC", "No se muestra informacion del WEBIRC en el /WHOIS para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WHOIS_SERVERNAME", "No se muestra el servidor en el /WHOIS para usuario", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WHOIS_IDLETIME", "No se muestra el idle en el /WHOIS para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WHOIS_LOCALCHAN", "No se muestran los canales locales en el /WHOIS para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WHO_SERVERNAME", "No se muestra el servidor en el /WHO para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_WHO_HOPCOUNT", "Se pone a 3 los saltos en el /WHO para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_MODEWHO", "No se muestra el servidor que realiza cambios de modos de canal", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_BANWHO", "No se muestra el servidor que pone el ban", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_KILLWHO", "No se muestra el servidor ni el oper que hace KILL", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_REWRITE", "Reescribe el servidor remoto al local en los num�ricos de respuesta", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_REMOTE", "No se permite consultas remotas para usuarios", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_NETSPLIT", "No se muestran los servidores en el net split", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("HIS_SERVERS", "Esconde los servidores y solo muestran services en el /MAP y /LINKS", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("HIS_SERVERNAME", "Nombre de servidor anónimo en el /WHOIS", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("HIS_SERVERINFO", "Descripción de servidor anónimo en el /WHOIS", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("HIS_URLSERVERS", "URL que mostrará en el /MAP y /LINK", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("NETWORK", "Nombre de la red", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("URL_CLIENTS", "URL para descargar cliente IRC compatible", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("URLREG", "URL para registrar el nick cuando se accede a canal +R", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("OPERSCHAN", "Canal donde están los opers disponibles para ayudar", TiposClaveTablaFeatures.CHANNEL, false),
                new ClavesTablaFeatures("DEBUGCHAN", "Canal a donde mandará los usuarios con +J", TiposClaveTablaFeatures.CHANNEL, false),
                new ClavesTablaFeatures("IP_CRYPT_KEY", "Clave de cifrado de IP's", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("UTF8_CONVERSION", "Conversión UTF-8 a Latin1 en los join de canal", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("MSG_FULLCAPACITY", "Mensaje cuando el servidor está lleno", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("ALLOW_RANDOM_NICKS", "Se permite poner nicks aleatorios con NICK *", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("PREFIX_RANDOM_NICKS", "Prefijo de los nicks aleatorios", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("ALLOW_SUSPEND_NICKS", "Se permite usar nicks suspendidos (+S)", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("REDIRECT_CHANNEL_ENABLE", "Habilita la redireccion de canales (tabla r)", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("GEO_ENABLE", "Activar control de usuarios por Geolocalización", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("GEO_MSG_KILL", "Mensaje de expulsión por lista negra Geolocalización", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("GEO_URL_VALIDATION", "Web para hacer la validación humana", TiposClaveTablaFeatures.STRING, false),
                new ClavesTablaFeatures("SPAM_OPER_COUNTDOWN", "Número de veces de spam para salir en SNOTICE", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SPAM_EXPIRE_TIME", "Segundos de expiración para ser un spam olvidado", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SPAM_JOINED_TIME", "Segundos que ha de pasar al join para ser spam", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SPAM_FJP_COUNT", "Número de JOIN/PART seguidos para ser spam", TiposClaveTablaFeatures.INT, true),
                new ClavesTablaFeatures("SPAM_CHECK_PRIVATES", "Se comprobará SPAM en privados", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("SPAM_CHECK_CHANNELS", "Se comprobará SPAM en canales", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("SPAM_CHECK_AWAYS", "Se comprobará SPAM en mensajes away", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("SPAM_CHECK_TOPICS", "Se comprobará SPAM en topicos", TiposClaveTablaFeatures.BOOL, false),
                new ClavesTablaFeatures("NETWORK_REHASH", "Permite hacer REHASH remoto", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("NETWORK_RESTART", "Permite hacer RESTART remoto", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("NETWORK_DIE", "Permite hacer DIE remoto", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_CERTFILE", "Nombre de archivo para certificado SSL", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("SSL_KEYFILE", "Nombre de archivo para la llave privada del certificado", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("SSL_CACERTFILE", "Nombre de archivo para los certificados CA válidos", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("SSL_VERIFYCERT", "Se comprueba el validez del certificado", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_NOSELFSIGNED", "No se permitirá la entrada de clientes con certificado autofirmado", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_REQUIRECLIENTCERT", "Se requiere un certificado cliente para entrar", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_NOSSLV2", "Se deshabilita el SSLv2", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_NOSSLV3", "Se deshabilita el SSLv3", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_NOTLSV1", "Se deshabilita el TLSv1", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("SSL_CIPHERS", "Lista de cifrados disponibles para las conexiones SSL", TiposClaveTablaFeatures.STRING, true),
                new ClavesTablaFeatures("CAP_multi_prefix", "Activa el soporte IRCv3 de multi-prefix (se manda todos los prefijos de status)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_userhost_in_names", "Activa el soporte IRCv3 de userhost-in-names (mostrar user@host en names)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_extended_join", "Activa el soporte IRCv3 de extended-join (notifica el account en el join)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_away_notify", "Activa el soporte IRCv3 de away-notify (notificar los aways)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_account_notify", "Activa el soporte IRCv3 de account-notify (notificar los account)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_invite_notify", "Activa el soporte IRCv3 de invite-notify (notificar los invites)", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_sasl", "Activa el soporte IRCv3 de SASL", TiposClaveTablaFeatures.BOOL, true),
                new ClavesTablaFeatures("CAP_tls", "Activa el soporte IRCv3 de TLS", TiposClaveTablaFeatures.BOOL, true)
            };
        }

        public void Carga(IRC irc)
        {
            this.irc = irc;

            RefrescarFeatures();
        }

        private void RefrescarFeatures()
        {
            dgvTablaFeatures.Rows.Clear();

            for (int i = 0; i < listaFeatures.Length; i++)
            {
                if (listaFeatures[i].onlyIRCh)
                    continue;

                RegistroDDB registro = irc.DDB.FindKey(TablasDDB.DDB_FEATUREDB, listaFeatures[i].feature);
                bool eliminar = false;

                if (registro != null) {
                    listaFeatures[i].valor = registro.Content;
                    eliminar = true;
                } else
                    listaFeatures[i].valor = string.Empty;

                dgvTablaFeatures.Rows.Add(listaFeatures[i].feature, listaFeatures[i].descripcionFeature, listaFeatures[i].valor, "", eliminar ? "Eliminar" : "");
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            RefrescarFeatures();
        }

        private void dgvTablaFeatures_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && dgvTablaFeatures.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Guardar")) {
                string key = dgvTablaFeatures.Rows[e.RowIndex].Cells[0].Value.ToString();
                string content = dgvTablaFeatures.Rows[e.RowIndex].Cells[2].Value.ToString();

                // Guardamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_FEATUREDB, key, content);
                dgvTablaFeatures.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaFeatures.Rows[e.RowIndex].Cells[4].Value = "Eliminar";
                MessageBox.Show("Se ha configurado el feature " + key, "Gestión de Features", MessageBoxButtons.OK);
            }
            else if (e.ColumnIndex == 4 && dgvTablaFeatures.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Eliminar"))
            {
                string key = dgvTablaFeatures.Rows[e.RowIndex].Cells[0].Value.ToString();
                // Eliminamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_FEATUREDB, key, null);
                dgvTablaFeatures.Rows[e.RowIndex].Cells[2].Value = "";
                dgvTablaFeatures.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaFeatures.Rows[e.RowIndex].Cells[4].Value = "";
                MessageBox.Show("Se ha deshabilitado el feature " + key, "Gestión de Features", MessageBoxButtons.OK);
            }
        }

        private void dgvTablaFeatures_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridViewCell celda = dgvTablaFeatures.Rows[e.RowIndex].Cells[e.ColumnIndex];

                /*
                // Comprobamos sintaxis #Canal
                if (celda.Value.ToString()[0] == '#')
                {*/
                    dgvTablaFeatures.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                    dgvTablaFeatures.Rows[e.RowIndex].Cells[3].Value = "Guardar";
                /*}
                else
                {
                    dgvTablaFeatures.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Formato incorrecto #canal";
                    dgvTablaFeatures.Rows[e.RowIndex].Cells[3].Value = "";
                }*/
            }
        }

        private void dgvTablaFeatures_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                dgvTablaFeatures.Rows[e.RowIndex].Cells[3].Value = "Guardar";
            }
        }
    }
}
