﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DDBGestionIRCh.Tablas
{
    public partial class Zonfig : UserControl
    {
        private ClavesTablaZonfig[] listaZonfig;
        private IRC irc;
        public Zonfig()
        {
            InitializeComponent();

            listaZonfig = new ClavesTablaZonfig[] 
            {
                new ClavesTablaZonfig("connexitdebugchan", "Eventos de entrada y salida de usuarios de la red"),
                new ClavesTablaZonfig("privsdebugchan", "Evento de uso de privilegios en el IRCD"),
                new ClavesTablaZonfig("geodebugchan", "Eventos del sistema de Geolocalización"),
                new ClavesTablaZonfig("spamdebugchan", "Eventos del control de AntiSpam")
            };
        }

        public void Carga(IRC irc)
        {
            this.irc = irc;

            RefrescarZonfig();
        }

        private void RefrescarZonfig()
        {
            dgvTablaZonfig.Rows.Clear();

            for (int i = 0; i < listaZonfig.Length; i++)
            {
                RegistroDDB registro = irc.DDB.FindKey(TablasDDB.DDB_ZONFIG, listaZonfig[i].tipoLogging);
                bool eliminar = false;

                if (registro != null) {
                    listaZonfig[i].canal = registro.Content;
                    eliminar = true;
                } else
                    listaZonfig[i].canal = string.Empty;

                dgvTablaZonfig.Rows.Add(listaZonfig[i].tipoLogging, listaZonfig[i].descripcionLogging, listaZonfig[i].canal, "", eliminar ? "Eliminar" : "");
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            RefrescarZonfig();
        }

        private void dgvTablaZonfig_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && dgvTablaZonfig.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Guardar")) {
                string key = dgvTablaZonfig.Rows[e.RowIndex].Cells[0].Value.ToString();
                string content = dgvTablaZonfig.Rows[e.RowIndex].Cells[2].Value.ToString();

                // Guardamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_ZONFIG, key, content);
                dgvTablaZonfig.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaZonfig.Rows[e.RowIndex].Cells[4].Value = "Eliminar";
                MessageBox.Show("Se ha logging de " + key, "Gestión de Zonfig", MessageBoxButtons.OK);
            }
            else if (e.ColumnIndex == 4 && dgvTablaZonfig.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Eliminar"))
            {
                string key = dgvTablaZonfig.Rows[e.RowIndex].Cells[0].Value.ToString();
                // Eliminamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_ZONFIG, key, null);
                dgvTablaZonfig.Rows[e.RowIndex].Cells[2].Value = "";
                dgvTablaZonfig.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaZonfig.Rows[e.RowIndex].Cells[4].Value = "";
                MessageBox.Show("Se ha desactivado el logging de " + key, "Gestión de Zonfig", MessageBoxButtons.OK);
            }
        }

        private void dgvTablaZonfig_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridViewCell celda = dgvTablaZonfig.Rows[e.RowIndex].Cells[e.ColumnIndex];

                // Comprobamos sintaxis #Canal
                if (celda.Value.ToString()[0] == '#')
                {
                    dgvTablaZonfig.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                    dgvTablaZonfig.Rows[e.RowIndex].Cells[3].Value = "Guardar";
                } else
                {
                    dgvTablaZonfig.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Formato incorrecto #canal";
                    dgvTablaZonfig.Rows[e.RowIndex].Cells[3].Value = "";
                }
            }
        }

        private void dgvTablaZonfig_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                dgvTablaZonfig.Rows[e.RowIndex].Cells[3].Value = "Guardar";
            }
        }
    }
}
