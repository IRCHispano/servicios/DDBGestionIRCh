﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DDBGestionIRCh.Tablas
{
    public partial class Bots : UserControl
    {
        private ClavesTablaBots[] listaBots;
        private IRC irc;
        public Bots()
        {
            InitializeComponent();

            listaBots = new ClavesTablaBots[] 
            {
                new ClavesTablaBots("NickServ", "Bot virtual de registro y protección de nicks"),
                new ClavesTablaBots("ChanServ", "Bot virtual de registro y control de canales"),
                new ClavesTablaBots("ClonesServ", "Bot virtual de control de Clones"),
                new ClavesTablaBots("SpamServ", "Bot virtual de control antispam")
            };
        }

        public void Carga(IRC irc)
        {
            this.irc = irc;

            RefrescarBots();
        }

        private void RefrescarBots()
        {
            dgvTablaBots.Rows.Clear();

            for (int i = 0; i < listaBots.Length; i++)
            {
                RegistroDDB registro = irc.DDB.FindKey(TablasDDB.DDB_BOTDB, listaBots[i].botVirtual);
                bool eliminar = false;

                if (registro != null) {
                    listaBots[i].mascara = registro.Content;
                    eliminar = true;
                } else
                    listaBots[i].mascara = string.Empty;

                dgvTablaBots.Rows.Add(listaBots[i].botVirtual, listaBots[i].descripcionBot, listaBots[i].mascara, "", eliminar ? "Eliminar" : "");
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            RefrescarBots();
        }

        private void dgvTablaBots_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && dgvTablaBots.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Guardar")) {
                string key = dgvTablaBots.Rows[e.RowIndex].Cells[0].Value.ToString();
                string content = dgvTablaBots.Rows[e.RowIndex].Cells[2].Value.ToString();

                // Guardamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_BOTDB, key, content);
                dgvTablaBots.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaBots.Rows[e.RowIndex].Cells[4].Value = "Eliminar";
                MessageBox.Show("Se ha activado el bot " + key, "Gestión de Bots", MessageBoxButtons.OK);
            }
            else if (e.ColumnIndex == 4 && dgvTablaBots.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Eliminar"))
            {
                string key = dgvTablaBots.Rows[e.RowIndex].Cells[0].Value.ToString();
                // Eliminamos el valor
                irc.DDB.DDB_Add(TablasDDB.DDB_BOTDB, key, null);
                dgvTablaBots.Rows[e.RowIndex].Cells[2].Value = "";
                dgvTablaBots.Rows[e.RowIndex].Cells[3].Value = "";
                dgvTablaBots.Rows[e.RowIndex].Cells[4].Value = "";
                MessageBox.Show("Se ha desactivado el bot " + key, "Gestión de Bots", MessageBoxButtons.OK);
            }
        }

        private void dgvTablaBots_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridViewCell celda = dgvTablaBots.Rows[e.RowIndex].Cells[e.ColumnIndex];

                // Comprobamos sintaxis Nick!user@Host
                string[] partesNick = celda.Value.ToString().Split('!');

                if (partesNick.Length == 2 && partesNick[0].Length > 0 && partesNick[1].Length > 0)
                {
                    string[] partesUser = partesNick[1].Split('@');
                    if (partesUser.Length == 2 && partesUser[0].Length > 0 && partesUser[1].Length > 0)
                    {
                        dgvTablaBots.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        dgvTablaBots.Rows[e.RowIndex].Cells[3].Value = "Guardar";
                        return;
                    }
                }
                dgvTablaBots.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Formato incorrecto nick!user@host";
                dgvTablaBots.Rows[e.RowIndex].Cells[3].Value = "";
            }
        }

        private void dgvTablaBots_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                dgvTablaBots.Rows[e.RowIndex].Cells[3].Value = "Guardar";
            }
        }
    }
}
