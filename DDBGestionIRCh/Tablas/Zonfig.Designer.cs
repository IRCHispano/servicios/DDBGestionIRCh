﻿namespace DDBGestionIRCh.Tablas
{
    partial class Zonfig
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.dgvTablaZonfig = new System.Windows.Forms.DataGridView();
            this.btnRefrescar = new System.Windows.Forms.Button();
            this.colTipoLogging = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCanal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGuardar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colEliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaZonfig)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(20, 17);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(699, 13);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "La tabla z de (Z)Config se gestionan los valores obsoletos en el IRCD de IRC-Hisp" +
    "ano. En el IRCh los siguientes valores se gestionarán en Logging";
            // 
            // dgvTablaZonfig
            // 
            this.dgvTablaZonfig.AllowUserToAddRows = false;
            this.dgvTablaZonfig.AllowUserToDeleteRows = false;
            this.dgvTablaZonfig.AllowUserToOrderColumns = true;
            this.dgvTablaZonfig.AllowUserToResizeColumns = false;
            this.dgvTablaZonfig.AllowUserToResizeRows = false;
            this.dgvTablaZonfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTablaZonfig.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTipoLogging,
            this.colDescripcion,
            this.colCanal,
            this.colGuardar,
            this.colEliminar});
            this.dgvTablaZonfig.Location = new System.Drawing.Point(23, 48);
            this.dgvTablaZonfig.MultiSelect = false;
            this.dgvTablaZonfig.Name = "dgvTablaZonfig";
            this.dgvTablaZonfig.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTablaZonfig.Size = new System.Drawing.Size(916, 150);
            this.dgvTablaZonfig.TabIndex = 6;
            this.dgvTablaZonfig.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaZonfig_CellClick);
            this.dgvTablaZonfig.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaZonfig_CellEndEdit);
            this.dgvTablaZonfig.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTablaZonfig_CellValueChanged);
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Location = new System.Drawing.Point(148, 222);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(88, 23);
            this.btnRefrescar.TabIndex = 7;
            this.btnRefrescar.Text = "REFRESCAR";
            this.btnRefrescar.UseVisualStyleBackColor = true;
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // colTipoLogging
            // 
            this.colTipoLogging.HeaderText = "Tipo Logging";
            this.colTipoLogging.Name = "colTipoLogging";
            this.colTipoLogging.ReadOnly = true;
            this.colTipoLogging.Width = 120;
            // 
            // colDescripcion
            // 
            this.colDescripcion.HeaderText = "Descripción";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            this.colDescripcion.Width = 480;
            // 
            // colCanal
            // 
            this.colCanal.HeaderText = "Canal";
            this.colCanal.MaxInputLength = 115;
            this.colCanal.Name = "colCanal";
            this.colCanal.ToolTipText = "Canal donde cantará los eventos";
            this.colCanal.Width = 170;
            // 
            // colGuardar
            // 
            this.colGuardar.HeaderText = "Guardar";
            this.colGuardar.Name = "colGuardar";
            this.colGuardar.Text = "Guardar";
            this.colGuardar.Width = 50;
            // 
            // colEliminar
            // 
            this.colEliminar.HeaderText = "Eliminar";
            this.colEliminar.Name = "colEliminar";
            this.colEliminar.Width = 50;
            // 
            // Zonfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRefrescar);
            this.Controls.Add(this.dgvTablaZonfig);
            this.Controls.Add(this.lblTitulo);
            this.Name = "Zonfig";
            this.Size = new System.Drawing.Size(952, 259);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaZonfig)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridView dgvTablaZonfig;
        private System.Windows.Forms.Button btnRefrescar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTipoLogging;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCanal;
        private System.Windows.Forms.DataGridViewButtonColumn colGuardar;
        private System.Windows.Forms.DataGridViewButtonColumn colEliminar;
    }
}
