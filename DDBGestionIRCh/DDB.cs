﻿/*
 * DDBGestionIRCh, DDB.cs
 *
 * Copyright (C) 2020 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing.Text;
using System.Windows.Forms;

namespace DDBGestionIRCh
{
    public class RegistroDDB
    {
        public string Mask;
        public string Key;
        public string Content;
    }

    public static class TablasDDB
    {
        public static readonly char DDB_INIT = 'a';
        public static readonly char DDB_BOTDB = 'b';
        public static readonly char DDB_CHANDB = 'c';
        public static readonly char DDB_EXCEPTIONDB = 'e';
        public static readonly char DDB_FEATUREDB = 'f';
        public static readonly char DDB_GEODB = 'g';
        public static readonly char DDB_ASNUM = 'h';
        public static readonly char DDB_ILINEDB = 'i';
        public static readonly char DDB_JUPEDB = 'j';
        public static readonly char DDB_LOGGINGDB = 'l';
        public static readonly char DDB_MOTDDB = 'm';
        public static readonly char DDB_NICKDB = 'n';
        public static readonly char DDB_OPERDB = 'o';
        public static readonly char DDB_PSEUDODB = 'p';
        public static readonly char DDB_QUARANTINEDB = 'q';
        public static readonly char DDB_CHANREDIRECTDB = 'r';
        public static readonly char DDB_SPAMDB = 's';
        public static readonly char DDB_UWORLDDB = 'u';
        public static readonly char DDB_VHOSTDB = 'v';
        public static readonly char DDB_WEBIRCDB = 'w';
        public static readonly char DDB_PROXYDB = 'y';
        public static readonly char DDB_ZONFIG = 'z';
        public static readonly char DDB_END = 'z';
    }
    public class DDB
    {
        private IRC irc;

        private List<RegistroDDB>[] data_table;
        private bool[] resident_table;
        //private ulong[] count_table; Con el List<>.Count podria sobrar esto
        public ulong[] id_table;
        //private uint[] hashtable_hi;
        //private uint[] hashtable_lo;
        
        public DDB(IRC irc)
        {
            this.irc = irc;

            data_table = new List<RegistroDDB>[256];
            resident_table = new bool[256];
            //count_table = new ulong[256];
            id_table = new ulong[256];
            //hashtable_hi = new uint[256];
            //hashtable_lo = new uint[256];

            // Habilitamos las tablas residentes y habilitamos la memoria
            InicializarTabla(TablasDDB.DDB_BOTDB);
            InicializarTabla(TablasDDB.DDB_CHANDB);
            InicializarTabla(TablasDDB.DDB_EXCEPTIONDB);
            InicializarTabla(TablasDDB.DDB_FEATUREDB);
            InicializarTabla(TablasDDB.DDB_GEODB);
            InicializarTabla(TablasDDB.DDB_ASNUM);
            InicializarTabla(TablasDDB.DDB_ILINEDB);
            InicializarTabla(TablasDDB.DDB_JUPEDB);
            InicializarTabla(TablasDDB.DDB_LOGGINGDB);
            InicializarTabla(TablasDDB.DDB_MOTDDB);
            InicializarTabla(TablasDDB.DDB_NICKDB);
            InicializarTabla(TablasDDB.DDB_OPERDB);
            InicializarTabla(TablasDDB.DDB_PSEUDODB);
            InicializarTabla(TablasDDB.DDB_QUARANTINEDB);
            InicializarTabla(TablasDDB.DDB_SPAMDB);
            InicializarTabla(TablasDDB.DDB_UWORLDDB);
            InicializarTabla(TablasDDB.DDB_VHOSTDB);
            InicializarTabla(TablasDDB.DDB_WEBIRCDB);
            InicializarTabla(TablasDDB.DDB_PROXYDB);
            InicializarTabla(TablasDDB.DDB_END);
        }

        private void InicializarTabla(char table)
        {
            data_table[table] = new List<RegistroDDB>();
            resident_table[table] = true;
        }

        public void DDB_Burst()
        {
            //La tabla n es especial
            irc.EnviarComando(string.Format("DB * 0 J {0} 2", id_table[TablasDDB.DDB_NICKDB]));

            for (char i = TablasDDB.DDB_INIT; i <= TablasDDB.DDB_END; i++)
            {
                if (i != TablasDDB.DDB_NICKDB)
                    irc.EnviarComando(string.Format("DB * 0 J {0} {1}", id_table[i], char.ToString(i)));
            }
        }

        public void DDB_Add(char table, string key, string content, string mask = "*")
        {
            string lineaDB;
            key = key.ToLower();
            ulong id = id_table[table] + 1;

            if (content != null && content.Length > 0)
                lineaDB = String.Format("DB * {0} {1} {2} :{3}", id, table, key, content);
            else
                lineaDB = String.Format("DB * {0} {1} {2}", id, table, key);

            irc.EnviarComando(string.Format(lineaDB));
            DDB_NewRegister(table, id, mask, key, content);
        }

        private void DDB_NewRegister(char table, ulong id, string mask, string key, string content)
        {
            // TODO: Calculo hash

            // TODO: Escritura en disco

            id_table[table] = id;

            // Si no es resident, no guardamos en memoria
            if (!resident_table[table])
                return;

            if (content != null)
                DDB_MemoryAdd(table, mask, key, content);
            else
                DDB_MemoryDel(table, mask, key);
        }

        private void DDB_MemoryAdd(char table, string mask, string key, string content)
        {
            List<RegistroDDB> lista = data_table[table];

            // Buscamos si existe el registro
            RegistroDDB registro = lista.Find(ddb => ddb.Key.Equals(key) && ddb.Mask.Equals(mask));

            if (registro != null)
            {
                //Es una actualizacion.
                registro.Content = content;
            }
            else
            {
                //Es un alta
                lista.Add(new RegistroDDB()
                {
                    Mask = mask,
                    Key = key,
                    Content = content
                });
            }
        }

        private void DDB_MemoryDel(char table, string mask, string key)
        {
            List<RegistroDDB> lista = data_table[table];

            lista.RemoveAll(ddb => ddb.Key.Equals(key) && ddb.Mask.Equals(mask));
        }

        public List<RegistroDDB> GetTabla(char table)
        {
            return data_table[table];
        }

        public RegistroDDB FindKey(char table, string key, string mask = "*")
        {
            key = key.ToLower();
            return data_table[table].Find(ddb => ddb.Key.Equals(key) && ddb.Mask.Equals(mask));
        }

        public void DDB_DB(string[] parametros)
        {
            // Parseamos los registros DB
            ulong id = ulong.Parse(parametros[3]);

            if (id == 0)
            {
                // COMANDOS
                /* Burst
                 * 0 X
                 * 1 DB (comando)
                 * 2 * (destino)
                 * 3 0 
                 * 4 J (Comando DB)
                 * 5 218 (serie)
                 * 6 2 (tabla, 2 para n)
                 */
                 // TODO: Pendiente
            }
            else
            {
                // Recepcion de regisstros
                /* Burst
                 * 0 X
                 * 1 DB (comando)
                 * 2 * (destino)
                 * 3 0 (serie)
                 * 4 n (tabla)
                 * 5 zoltan (key)
                 * 6 { xxxx } (contenido)
                 */
                char table = parametros[4][0];

                if (table < TablasDDB.DDB_INIT || table > TablasDDB.DDB_END)
                {
                    if (table == 'N')
                        table = TablasDDB.DDB_NICKDB;
                    else
                        return;
                }

                // Rechazamos registros con id inferior a lo que tenemos
                if (id <= id_table[table])
                    return;

                if (!parametros[5].Equals("*"))
                    DDB_NewRegister(table, id, parametros[2], parametros[5], parametros.Length > 6 ? parametros[6] : null);
                else
                {
                    ; //TODO: Compactado DDB
                }

            }

        }
    }
}
