﻿/*
 * DDBGestionIRCh, frmMain.cs
 *
 * Copyright (C) 2020 Toni Garcia - zoltan <toni@tonigarcia.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
using DDBGestionIRCh.Properties;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace DDBGestionIRCh
{
    public partial class frmMain : Form
    {
        private IRC irc;
        private bool conectado = false;
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //Titulo con el nombre y version.
            this.Text = String.Format("Gestion de DDB para IRCh & IRCd de IRC-Hispano. Versión {0}. Fecha {1}",
                Application.ProductVersion, File.GetLastWriteTime(Assembly.GetEntryAssembly().Location).ToShortDateString());

            txtNombreServidor.Text = Settings.Default.NombreServidor;
            txtHostServidor.Text = Settings.Default.HostServidor;
            txtPuerto.Text = Settings.Default.PuertoServidor.ToString();
            txtPassword.Text = Settings.Default.PasswordServidor.ToString();
            txtNumerico.Text = Settings.Default.Numerico.ToString();

            VisibilidadBotonesConexion(false);

            this.irc = new IRC();

            irc.CambioEstadoIRC += Irc_CambioEstadoIRC;
            irc.MsgIn += Irc_MsgIn;
            irc.MsgOut += Irc_MsgOut;
        }



        #region "Eventos Grupo Conexion"
        private void txtNombreServidor_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombreServidor.Text))
            {
                errorProvider.SetError(txtNombreServidor, "El nombre de servidor es obligatorio");
                txtNombreServidor.Tag = false;
                txtNombreServidor.Focus();
            }
            else
            {
                errorProvider.SetError(txtNombreServidor, "");
                txtNombreServidor.Tag = true;
            }
        }

        private void txtHostServidor_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtHostServidor.Text))
            {
                errorProvider.SetError(txtHostServidor, "El host/IP del servidor HUB es obligatorio");
                txtHostServidor.Tag = false;
                txtHostServidor.Focus();
            }
            else
            {
                errorProvider.SetError(txtHostServidor, "");
                txtHostServidor.Tag = true;
            }
        }

        private void txtPuerto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtPuerto_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPuerto.Text))
            {
                errorProvider.SetError(txtPuerto, "El puerto es obligatorio, suele ser el 4400");
                txtPuerto.Tag = false;
                txtPuerto.Focus();
                return;
            }
            int puerto = Int32.Parse(txtPuerto.Text);
            if (puerto < 0 || puerto > 65535)
            {
                errorProvider.SetError(txtPuerto, "El puerto tiene que ser entre 0 y 65535");
                txtPuerto.Tag = false;
                txtPuerto.Focus();
            }
            else
            {
                errorProvider.SetError(txtPuerto, "");
                txtPuerto.Tag = true;
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                errorProvider.SetError(txtPassword, "La contraseña de la conexión con el servidor HUB es obligatoria");
                txtPassword.Tag = false;
                txtPassword.Focus();
            }
            else
            {
                errorProvider.SetError(txtPassword, "");
                txtPassword.Tag = true;
            }
        }

        private void txtNumerico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtNumerico_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNumerico.Text))
            {
                errorProvider.SetError(txtNumerico, "El numérico es obligatorio.");
                txtNumerico.Tag = false;
                txtNumerico.Focus();
                return;
            }
            int numerico = Int32.Parse(txtNumerico.Text);
            if (numerico < 0 || numerico > 4095)
            {
                errorProvider.SetError(txtNumerico, "El numérico tiene que ser entre 0 y 4095");
                txtPuerto.Tag = false;
                txtPuerto.Focus();
            }
            else
            {
                errorProvider.SetError(txtNumerico, "");
                txtPuerto.Tag = true;
            }
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombreServidor.Text) || string.IsNullOrWhiteSpace(txtHostServidor.Text)
                || string.IsNullOrWhiteSpace(txtPuerto.Text) || string.IsNullOrWhiteSpace(txtPassword.Text)
                || string.IsNullOrWhiteSpace(txtNumerico.Text)) {
                MessageBox.Show("Todos los campos de conexión son obligatorios.", "Conexion a la red IRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            btnConectar.Enabled = false;
            Settings.Default.Save();
            conectado = this.irc.ConectarIRC(txtNombreServidor.Text, txtHostServidor.Text, Int16.Parse(txtPuerto.Text), txtPassword.Text, Int16.Parse(txtNumerico.Text));

            if (!conectado)
            {
                lblEstado.Text = String.Format("ERROR de Conexion: {0}", irc.UltimoError);
                toolTip.SetToolTip(lblEstado, irc.UltimoError);
            }
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            irc.DesconectarIRC();
        }
        #endregion

        #region "Eventos de IRC"
        private void Irc_CambioEstadoIRC(EstadoIRC estado, string msg)
        {
            if (estado == EstadoIRC.Listo)
                conectado = true;
            else
                conectado = false;

            VisibilidadBotonesConexion(conectado);
            lblEstado.Text = msg;
            toolTip.SetToolTip(lblEstado, msg);
        }
        private void Irc_MsgOut(string msg)
        {
            throw new NotImplementedException();
        }

        private void Irc_MsgIn(string msg)
        {
            throw new NotImplementedException();
        }
        #endregion




        private void VisibilidadBotonesConexion(bool conectado)
        {
            btnConectar.Enabled = !conectado;
            btnDesconectar.Enabled = conectado;

            tabControl1.Visible = conectado;

            if (conectado)
                HabilitarFormularios();
        }




        private void CompletarLineaDB()
        {
            if (txtTabla.Text.Length > 0 && txtClave.Text.Length > 0)
            {
                ulong numSerie = irc.GetNumeroSerieTabla(txtTabla.Text[0]) + 1;
                lblEnviar.Text = String.Format("DB * {0} {1} {2} :{3}", numSerie, txtTabla.Text,  txtClave.Text, txtContenido.Text);
            }
        }

        private void txtClave_TextChanged(object sender, EventArgs e)
        {
            CompletarLineaDB();
        }

        private void txtTabla_TextChanged(object sender, EventArgs e)
        {
            CompletarLineaDB();
        }

        private void txtTabla_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 'a') || (e.KeyChar > 'z'))
            {
                e.Handled = true;
            }
        }

        private void txtContenido_TextChanged(object sender, EventArgs e)
        {
            CompletarLineaDB();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            irc.DDB.DDB_Add(txtTabla.Text[0], txtClave.Text, txtContenido.Text);
            txtTabla.Clear();
            txtClave.Clear();
            txtContenido.Clear();
            lblEnviar.Text = "";
        }

        private void tablaBots_Load(object sender, EventArgs e)
        {
            
        }

        private void HabilitarFormularios()
        {
            tablaBots.Carga(this.irc);
            tablaFeatures.Carga(this.irc);
            tablaZonfig.Carga(this.irc);
        }
    }
}
