# Changelog
Todos los cambios notables de este proyecto deben ser documentados en este archivo.

El formato esta basado en [Mantenga un ChangeLog](https://keepachangelog.com/es-ES/1.0.0/),
y este proyecto se adhiere a [Versionado Semántico](https://semver.org/lang/es).


## [0.1.0] - 2020-05-12 - toni@tonigarcia.es

### Added
 - Primera version inicial, simplemente manda una linea DB.